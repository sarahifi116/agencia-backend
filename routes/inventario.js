const {connection} = require('../config.db.js');
const {STATUS_CODE} = require('../constants.js');
const {idSchema} = require('../validators/common.schema.js');
const {inventarioSchema} = require('../validators/inventario.schema.js');

const express = require('express');
const app = express();

const getAll = (request, response) => {
    connection.query(`SELECT a.id, a.kilometraje, DATE_FORMAT(a.fechaIngreso, '%Y-%m-%d') as fechaIngreso, a.idModelo, b.nombre as modelo, c.id as idMarca, c.nombre as marca, e.id as idTipo, e.nombre as tipo, a.idColor, d.nombre as color FROM inventario a inner join modelos b on a.idModelo = b.id inner join marcas c on b.idMarca = c.id inner join colores d on a.idColor = d.id inner join tipos e on b.idTipo = e.id`, 
    (error, results) => {
        if (error) {
            return response.status(STATUS_CODE.INTERNAL_SERVER_ERROR)
                .json(error)
                .end();
        }

        return response.status(STATUS_CODE.OK)
            .json(results)
            .end();
    });
};

const getById = (request, response) => {
    const {error, value} = idSchema.validate(request.params.id);

    if (error) {
        return response.status(STATUS_CODE.INTERNAL_SERVER_ERROR)
            .json(error.message)
            .end();
    }

    connection.query('SELECT * FROM inventario WHERE id = ?', 
    [value],
    (error, results) => {
        if (error) {
            return response.status(STATUS_CODE.INTERNAL_SERVER_ERROR)
                .json(error)
                .end();
        }

        return response.status(STATUS_CODE.OK)
            .json(results)
            .end();
    });
};

const create = (request, response) => {
    const {error, value} = inventarioSchema.validate(request.body);

    if (error) {
        return response.status(STATUS_CODE.INTERNAL_SERVER_ERROR)
            .json(error.message)
            .end();
    }

    const {idModelo, idColor, kilometraje, fechaIngreso} = value;

    connection.query('INSERT INTO inventario(idModelo, idColor, kilometraje, fechaIngreso) VALUES (?, ?, ?, ?)', 
    [idModelo, idColor, kilometraje, fechaIngreso],
    (error, results) => {
        if (error) {
            return response.status(STATUS_CODE.INTERNAL_SERVER_ERROR)
                .json(error)
                .end();
        }

        return response.status(STATUS_CODE.CREATED)
            .json({'Item agregado al inventario correctamente': results.affectedRows})
            .end();
    });
};

const update = (request, response) => {
    const idValidationResult = idSchema.validate(request.params.id);
    
    const bodyValidationResult = inventarioSchema.validate(request.body);
    
    if (idValidationResult.error || bodyValidationResult.error) {
        message = (idValidationResult.error) ? idValidationResult.error.message : bodyValidationResult.error.message;

        return response.status(STATUS_CODE.INTERNAL_SERVER_ERROR)
            .json(message)
            .end();
    }

    const {idModelo, idColor, kilometraje, fechaIngreso} = bodyValidationResult.value;

    connection.query('UPDATE inventario SET idModelo = ?, idColor = ?, kilometraje = ?, fechaIngreso = ? WHERE id = ?', 
    [idModelo, idColor, kilometraje, fechaIngreso, idValidationResult.value],
    (error, results) => {
        if (error) {
            return response.status(STATUS_CODE.INTERNAL_SERVER_ERROR)
                .json(error)
                .end();
        }

        return response.status(STATUS_CODE.CREATED)
            .json({'Inventario actualizado correctamente': results.affectedRows})
            .end();
    });
};

const remove = (request, response) => {
    const {error, value} = idSchema.validate(request.params.id);

    if (error) {
        return response.status(STATUS_CODE.INTERNAL_SERVER_ERROR)
            .json(error.message)
            .end();
    }

    connection.query('DELETE FROM inventario WHERE id = ?', 
    [value],
    (error, results) => {
        if (error) {
            return response.status(STATUS_CODE.INTERNAL_SERVER_ERROR)
                .json(error)
                .end();
        }

        return response.status(STATUS_CODE.OK)
            .json({'eliminado correctamente': results.affectedRows})
            .end();
    });35
};

app.route('/inventario')
    .get(getAll);

app.route('/inventario/:id')
    .get(getById);

app.route('/inventario')
    .post(create);

app.route('/inventario/:id')
    .put(update);

app.route('/inventario/:id')
    .delete(remove);

module.exports = app;