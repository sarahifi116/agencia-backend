const {connection} = require('../config.db.js');
const {STATUS_CODE} = require('../constants.js');

const express = require('express');
const app = express();

const getAll = (request, response) => {
    connection.query('SELECT * FROM tipos', 
    (error, results) => {
        if (error) {
            return response.status(STATUS_CODE.INTERNAL_SERVER_ERROR)
                .json(error)
                .end();
        }

        return response.status(STATUS_CODE.OK)
            .json(results)
            .end();
    });
};

app.route('/tipos')
.get(getAll);

module.exports = app;