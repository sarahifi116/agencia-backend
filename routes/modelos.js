const {connection} = require('../config.db.js');
const {STATUS_CODE} = require('../constants.js');
const {idSchema} = require('../validators/common.schema.js');
const {modelosSchema} = require('../validators/modelos.schema.js');

const express = require('express');
const app = express();

const getAll = (request, response) => {
    connection.query('SELECT a.id, a.nombre, a.idTipo, a.idMarca, b.nombre as marca, c.nombre as tipo FROM modelos a inner join marcas b on a.idMarca = b.id inner join tipos c on a.idTipo = c.id', 
    (error, results) => {
        if (error) {
            return response.status(STATUS_CODE.INTERNAL_SERVER_ERROR)
                .json(error)
                .end();
        }

        return response.status(STATUS_CODE.OK)
            .json(results)
            .end();
    });
};

const getById = (request, response) => {
    const {error, value} = idSchema.validate(request.params.id);

    if (error) {
        return response.status(STATUS_CODE.INTERNAL_SERVER_ERROR)
            .json(error.message)
            .end();
    }

    connection.query('SELECT * FROM modelos WHERE id = ?', 
    [value],
    (error, results) => {
        if (error) {
            return response.status(STATUS_CODE.INTERNAL_SERVER_ERROR)
                .json(error)
                .end();
        }

        return response.status(STATUS_CODE.OK)
            .json(results)
            .end();
    });
};

const create = (request, response) => {
    const {error, value} = modelosSchema.validate(request.body);

    if (error) {
        return response.status(STATUS_CODE.INTERNAL_SERVER_ERROR)
            .json(error.message)
            .end();
    }

    const {nombre, idTipo, idMarca} = value;

    connection.query('INSERT INTO modelos(nombre, idTipo, idMarca) VALUES (?, ?, ?)', 
    [nombre, idTipo, idMarca],
    (error, results) => {
        if (error) {
            return response.status(STATUS_CODE.INTERNAL_SERVER_ERROR)
                .json(error)
                .end();
        }

        return response.status(STATUS_CODE.CREATED)
            .json({'Modelo agregado correctamente': results.affectedRows})
            .end();
    });
};

const update = (request, response) => {
    const idValidationResult = idSchema.validate(request.params.id);
    
    const bodyValidationResult = modelosSchema.validate(request.body);
    
    if (idValidationResult.error || bodyValidationResult.error) {
        message = (idValidationResult.error) ? idValidationResult.error.message : bodyValidationResult.error.message;

        return response.status(STATUS_CODE.INTERNAL_SERVER_ERROR)
            .json(message)
            .end();
    }
    
    const {nombre, idTipo, idMarca} = bodyValidationResult.value;

    connection.query('UPDATE modelos SET nombre = ?, idTipo = ?, idMarca = ? WHERE id = ?', 
    [nombre, idTipo, idMarca, idValidationResult.value],
    (error, results) => {
        if (error) {
            return response.status(STATUS_CODE.INTERNAL_SERVER_ERROR)
                .json(error)
                .end();
        }

        return response.status(STATUS_CODE.CREATED)
            .json({'Modelo actualizado correctamente': results.affectedRows})
            .end();
    });
};

const remove = (request, response) => {
    const {error, value} = idSchema.validate(request.params.id);

    if (error) {
        return response.status(STATUS_CODE.INTERNAL_SERVER_ERROR)
            .json(error.message)
            .end();
    }

    connection.query('DELETE FROM modelos WHERE id = ?', 
    [value],
    (error, results) => {
        if (error) {
            return response.status(STATUS_CODE.INTERNAL_SERVER_ERROR)
                .json(error)
                .end();
        }

        return response.status(STATUS_CODE.OK)
            .json({'Modelo eliminado correctamente': results.affectedRows})
            .end();
    });
};

app.route('/modelos')
    .get(getAll);

app.route('/modelos/:id')
    .get(getById);

app.route('/modelos')
    .post(create);

app.route('/modelos/:id')
    .put(update);

app.route('/modelos/:id')
    .delete(remove);

module.exports = app;