const {connection} = require('../config.db.js');
const {STATUS_CODE} = require('../constants.js');
const {idSchema} = require('../validators/common.schema.js');
const {marcasSchema} = require('../validators/marcas.schema.js');

const express = require('express');
const app = express();

const getAll = (request, response) => {
    connection.query('SELECT * FROM marcas', 
    (error, results) => {
        if (error) {
            return response.status(STATUS_CODE.INTERNAL_SERVER_ERROR)
                .json(error)
                .end();
        }

        return response.status(STATUS_CODE.OK)
            .json(results)
            .end();
    });
};

const getById = (request, response) => {
    const {error, value} = idSchema.validate(request.params.id);

    if (error) {
        return response.status(STATUS_CODE.INTERNAL_SERVER_ERROR)
            .json(error.message)
            .end();
    }

    connection.query('SELECT * FROM marcas WHERE id = ?', 
    [value],
    (error, results) => {
        if (error) {
            return response.status(STATUS_CODE.INTERNAL_SERVER_ERROR)
                .json(error)
                .end();
        }

        return response.status(STATUS_CODE.OK)
            .json(results)
            .end();
    });
};

const create = (request, response) => {
    const {error, value} = marcasSchema.validate(request.body);

    if (error) {
        return response.status(STATUS_CODE.INTERNAL_SERVER_ERROR)
            .json(error.message)
            .end();
    }

    const {nombre} = value;

    connection.query('INSERT INTO marcas(nombre) VALUES (?)', 
    [nombre],
    (error, results) => {
        if (error) {
            return response.status(STATUS_CODE.INTERNAL_SERVER_ERROR)
                .json(error)
                .end();
        }

        return response.status(STATUS_CODE.CREATED)
            .json({'Marca agregada correctamente': results.affectedRows})
            .end();
    });
};

const update = (request, response) => {
    const idValidationResult = idSchema.validate(request.params.id);
    
    const bodyValidationResult = marcasSchema.validate(request.body);
    
    if (idValidationResult.error || bodyValidationResult.error) {
        message = (idValidationResult.error) ? idValidationResult.error.message : bodyValidationResult.error.message;

        return response.status(STATUS_CODE.INTERNAL_SERVER_ERROR)
            .json(message)
            .end();
    }

    const {nombre} = bodyValidationResult.value;

    connection.query('UPDATE marcas SET nombre = ? WHERE id = ?', 
    [nombre, idValidationResult.value],
    (error, results) => {
        if (error) {
            return response.status(STATUS_CODE.INTERNAL_SERVER_ERROR)
                .json(error)
                .end();
        }

        return response.status(STATUS_CODE.CREATED)
            .json({'Marca actualizada correctamente': results.affectedRows})
            .end();
    });
};

const remove = (request, response) => {
    const {error, value} = idSchema.validate(request.params.id);

    if (error) {
        return response.status(STATUS_CODE.INTERNAL_SERVER_ERROR)
            .json(error.message)
            .end();
    }

    connection.query('DELETE FROM marcas WHERE id = ?', 
    [value],
    (error, results) => {
        if (error) {
            return response.status(STATUS_CODE.INTERNAL_SERVER_ERROR)
                .json(error)
                .end();
        }

        return response.status(STATUS_CODE.OK)
            .json({'Marca eliminada correctamente': results.affectedRows})
            .end();
    });
};

app.route('/marcas')
    .get(getAll);

app.route('/marcas/:id')
    .get(getById);

app.route('/marcas')
    .post(create);

app.route('/marcas/:id')
    .put(update);

app.route('/marcas/:id')
    .delete(remove);

module.exports = app;