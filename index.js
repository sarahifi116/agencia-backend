const cors = require('cors');
const express = require('express');
const app = express();

const {DEFAULT_PORT} = require('./constants.js');

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use(require('./routes/marcas.js'));
app.use(require('./routes/modelos.js'));
app.use(require('./routes/tipos.js'));
app.use(require('./routes/colores.js'));
app.use(require('./routes/inventario.js'));

app.listen(process.env.PORT||DEFAULT_PORT, () => {
    if (process.env.PORT) {
        console.log(`Servidor corriendo en el puerto ${process.env.PORT}`);
    } else {
        console.log(`Servidor corriendo en el puerto ${DEFAULT_PORT}`);
    }
});

module.exports = app;
