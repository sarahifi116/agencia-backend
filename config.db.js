//dotenv nos permite leer las variables de entorno de nuestro .env
const dotenv = require("dotenv");
dotenv.config();

const {DATABASE_CREDENTIALS} = require('./constants.js');
const mysql = require('mysql');
let connection;

try {
    connection = mysql.createConnection(DATABASE_CREDENTIALS);
} catch (error) {
    console.log("Error al conectar con la base de datos");
}

module.exports = {connection};