# NodeJs Backend

### Prerequisitos

Tener instalado:
- NodeJs & NPM
- MySQL Client en ejecucion
- Haber ejecutado el script de la base de datos

Nota: Si las credenciales de la base de datos de tu ambiente no coinciden, actualizar el .env por las correctas.


### Run project
- Instala node_modules: `npm install`
- Ejecutar la API: `npm start`

Para mas referencias ve a `package.json`



#### Misc
//SELECT user,authentication_string,plugin,host FROM mysql.user where user='root';
//create user 'root'@'%' identified by 'root';grant all on *.* to 'root'@'%';
//ALTER USER 'root'@'localhost' IDENTIFIED BY 'root'; 
//ALTER USER 'root'@'%' IDENTIFIED WITH mysql_native_password BY 'root';
// FLUSH PRIVILEGES;