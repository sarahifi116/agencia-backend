const Joi = require('joi');

const {nombreSchema} = require('./common.schema.js');

const marcasSchema = Joi.object({
    nombre: nombreSchema,
});

module.exports = {
    marcasSchema,
}