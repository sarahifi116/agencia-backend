const Joi = require('joi');

const {nombreSchema} = require('./common.schema.js');

const modelosSchema = Joi.object({
    nombre: nombreSchema,
    idTipo: Joi.number()
        .positive()
        .required()
        .label('idTipo'),
    idMarca: Joi.number()
        .positive()
        .required()
        .label('idMarca'),
});

module.exports = {
    modelosSchema,
}