const Joi = require('joi');

const inventarioSchema = Joi.object({
    idModelo: Joi.number()
        .positive()
        .required()
        .label('idModelo'), 
    idColor: Joi.number()
        .positive()
        .required()
        .label('idColor'), 
    kilometraje: Joi.string()
        .pattern(new RegExp('^[0-9]{1,15}$'))
        .required()
        .label('kilometraje'),
    fechaIngreso: Joi.string()
        .required()
        .label('fechaIngreso'),
});

module.exports = {
    inventarioSchema,
}