const Joi = require('joi');

const idSchema = Joi.number()
    .positive()
    .required()
    .label('id');

const nombreSchema = Joi.string()
    .max(100)
    .required()
    .label('nombre');

module.exports = {
    idSchema,
    nombreSchema,
}