const dotenv = require("dotenv");
dotenv.config();

const DEFAULT_PORT = 3300;

const DATABASE_CREDENTIALS = {
    host: process.env.DBHOST,
    port: process.env.DBPORT,
    user: process.env.DBUSER,
    password: process.env.DBPASS,
    database: process.env.DBNAME,
};

const STATUS_CODE = {
    INTERNAL_SERVER_ERROR: 500,
    NOT_FOUND: 404,
    CREATED: 201,
    OK: 200,
}

module.exports = {
    DEFAULT_PORT,
    DATABASE_CREDENTIALS,
    STATUS_CODE,
}
