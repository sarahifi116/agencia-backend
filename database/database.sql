create database agencia;

use agencia;

create table marcas(
    id int primary key auto_increment,
    nombre varchar(100) not null
);

create table tipos(
    id int primary key auto_increment,
    nombre varchar(100) not null
);

create table colores(
    id int primary key auto_increment,
    nombre varchar(100) not null
);

create table modelos(
    id int primary key auto_increment,
    nombre varchar(100) not null,
    idTipo int not null references tipos(id),
    idMarca int not null references marcas(id)
);

create table inventario(
    id int primary key auto_increment,
    kilometraje varchar(30) not null,
    fechaIngreso date not null,
    idModelo int not null references modelos(id),
    idColor int not null references colores(id)
);

insert into colores (nombre) values ('Rojo');
insert into colores (nombre) values ('Amarillo');
insert into colores (nombre) values ('Azul');
insert into colores (nombre) values ('Plateado');
insert into colores (nombre) values ('Negro');

insert into tipos (nombre) values ('Sedan');
insert into tipos (nombre) values ('Deportivo');
insert into tipos (nombre) values ('Pickup');
insert into tipos (nombre) values ('SUV');
insert into tipos (nombre) values ('Van');
